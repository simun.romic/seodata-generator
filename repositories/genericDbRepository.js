const dynamodb = require('../db/dynamoDb');

/**
 * Insert item into table
 * @param {String} table
 * @param {String} id
 * @param {Object} item
 */
const create = (table, id, item) => {
  const params = {
    TableName: table,
    Item: {
      id,
      ...item,
    },
  };

  return dynamodb.put(params).promise();
};

/**
 * Get item from table
 * @param {String} table
 * @param {String} id
 */
const get = (table, id) => {
  const params = {
    TableName: table,
    Key: {
      id,
    },
  };

  return dynamodb.get(params).promise();
};

/**
 * Delete item from table
 * @param {String} table
 * @param {String} id
 */
const ddelete = (table, id) => {
  const params = {
    TableName: table,
    Key: {
      id,
    },
  };

  return dynamodb.delete(params).promise();
};

/**
 * Update item in table
 * @param {String} params
 */
const update = params => dynamodb.update(params).promise();

/**
 * Query item against GSI
 * @param {Object} params
 */
const query = params => dynamodb.query(params).promise();

module.exports = {
  get,
  update,
  _delete: ddelete,
  create,
  query,
};
