const { SEODATA_TABLE } = process.env;
const { create, get } = require('./genericDbRepository');

/**
 * insertOrUpdate seo data into db
 * @param {Object} obj
 */
const save = async obj => {
  const seoData = obj;
  const timestamp = new Date().getTime();
  seoData.createdAt = timestamp;
  seoData.updatedAt = timestamp;

  return create(SEODATA_TABLE, seoData.id, seoData);
};

/**
 * Get seo data by id
 * @param {String} id
 */
const getById = async id => {
  const seoDataItem = await get(SEODATA_TABLE, id);

  const seoData = seoDataItem && seoDataItem.Item ? seoDataItem.Item : {};
  return seoData;
};

module.exports = {
  save,
  getById,
};
