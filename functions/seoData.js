const seoDataRepository = require('../repositories/seoDataRepository');
const { success, internalServerError, badRequest } = require('../utils/responses');
const { isEmpty } = require('../utils/objectUtils');
const { validate } = require('../validators/schemaValidator');

/**
 * Generate SEO data template based on seo data object
 * @param {Object} seoData
 */
const generateSeoDataTemplate = seoData => { return {
    head: `<link rel='canonical' href='${seoData.protocol}://${seoData.hostname}${seoData.pathname}'>
          <link rel='alternate' href='${seoData.protocol}://${seoData.hostname}${seoData.pathname}' hreflang='x-default'>
          <link rel='alternate' href='${seoData.protocol}://${seoData.hostname}${seoData.pathname}' hreflang='en-us'>
          <script type='application/ld+json'>
            {
              '@context': 'http://schema.org',
              '@type': 'WebSite',
              'name': '${seoData.hostname}',
              'alternateName': '${seoData.hostname}',
              'url': '${seoData.protocol}://${seoData.hostname}${seoData.pathname}'
            }
          </script>`,
    body: ''
  };
};

/**
 * Hanlder for generating seo data
 * @param {Object} event
 */
const generate = async event => {
  // Validate JSON
  const result = validate(event.body, 'seo-data');
  if (result.error) {
    return badRequest(result.error);
  }

  try {
    const { protocol, hostname, pathname, parameters, indexable } = JSON.parse(event.body);  
    const id = hostname + pathname;
    let seoData = await seoDataRepository.getById(id);

    if (isEmpty(seoData)) {
      seoData = {
        id,
        protocol,
        hostname,
        pathname,
        parameters, 
        indexable,
      }

      await seoDataRepository.save(seoData);
    }

    console.log(`seoData: ${JSON.stringify(seoData)}`);
    return success(generateSeoDataTemplate(seoData));
  } catch (err) {
    console.log(`Error while saving user data: ${err}`);
    return internalServerError();
  }
};

module.exports = {
  generate,
};
