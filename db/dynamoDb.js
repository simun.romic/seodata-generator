// eslint-disable-next-line import/no-extraneous-dependencies
const AWS = require('aws-sdk');
const https = require('https');

/**
 * Enable keep alive HTTP connections for DynamoDB client
 * so we can reuse existing TCP connections when making calls to DyamoDB
 * Performance optimization
 */
const agent = new https.Agent({
  maxSockets: 50,
  keepAlive: true,
  rejectUnauthorized: true,
});

let options = {
  httpOptions: {
    agent,
  },
};

// connect to local DB if running offline
if (process.env.IS_OFFLINE) {
  options = {
    region: 'localhost',
    endpoint: 'http://localhost:8000',
  };
}

const client = new AWS.DynamoDB.DocumentClient(options);

module.exports = client;
