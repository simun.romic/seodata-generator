# seoGenerator

API function for generating SEO data

## Requirements

- install aws cli - https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html
- setup aws account - https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html#cli-quick-configuration

## Running locally

- install jdk
- run `npm i` -> this will install project dependencies
- run `npm run install:dynamodb` -> this will install dynamodb locally
- run `npm run start:local` -> this will start local dynamodb instance and create table based on table resource definitions and populate table from seeds if they are enabled and hosts all api endpoints on localhost and port defined in `serverless.yml` file
- if need to reinstall local dynamodb run: `npm run reinstall:dynamodb`
- more information on local dynamodb: https://github.com/99xt/serverless-dynamodb-local

## Deployment

- run `npm run deploy:dev` this will deploy functions in `dev` stage, for production use `npm run deploy:prod`

## Deleting whole stack

- run `npm run delete:dev` this will delete all resources creted on AWS related to this app in `dev` stage, for production use `npm run deploy:prod`
