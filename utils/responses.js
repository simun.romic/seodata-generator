const httpStatus = {
  OK: 200,
  INTERNAL_SERVER_ERROR: 500,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
};

const headers = {
  'Content-Type': 'application/json',
  /* Required for CORS support to work */
  'Access-Control-Allow-Origin': '*',
  /* Required for cookies, authorization headers with HTTPS */
  'Access-Control-Allow-Credentials': true,
};

module.exports = {
  success: (data = { status: 'OK' }) => ({
    statusCode: httpStatus.OK,
    headers,
    body: JSON.stringify(data, null, 2).replace(/\\n/g, ''),
  }),

  emptySucess: () => ({
    statusCode: httpStatus.OK,
    headers,
    body: '',
  }),

  internalServerError: () => ({
    statusCode: httpStatus.INTERNAL_SERVER_ERROR,
    headers,
    body: JSON.stringify({ error: 'INTERNAL_SERVER_ERROR' }),
  }),

  badRequest: (data = { status: 'BAD_REQUEST' }) => ({
    statusCode: httpStatus.BAD_REQUEST,
    headers,
    body: JSON.stringify(data, null, 2),
  }),

  unauthorized: () => ({
    statusCode: httpStatus.UNAUTHORIZED,
    headers,
    body: JSON.stringify({ status: 'UNAUTHORIZED' }),
  }),

  forbidden: () => ({
    statusCode: httpStatus.FORBIDDEN,
    headers,
    body: JSON.stringify({ status: 'FORBIDDEN' }),
  }),
};
