/**
 * NONE - If ReturnValues is not specified, or if its value is NONE, then nothing is returned.
 * ALL_OLD - Returns all of the attributes of the item, as they appeared before update operation.
 * UPDATED_OLD - Returns only the updated attributes, as they appeared before update operation.
 * ALL_NEW - Returns all of the attributes of the item, as they appear after update operation.
 * UPDATED_NEW - Returns only the updated attributes, as they appear after the update operation.
 */
const RETURN_VALUES = Object.freeze({
  NONE: 'NONE',
  ALL_OLD: 'ALL_OLD',
  UPDATED_OLD: 'UPDATED_OLD',
  ALL_NEW: 'ALL_NEW',
  UPDATED_NEW: 'ALL_NEW',
});

module.exports = { RETURN_VALUES };
