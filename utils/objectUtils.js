/**
 * Return true if object is empty: {} | null | undefiend
 * @param {Object} obj 
 */
const isEmpty = (obj) => {
    if (!obj) {
        return true;
    }

    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

module.exports = { isEmpty };