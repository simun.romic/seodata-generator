const Joi = require('joi');

const schemas = {
  'seo-data': Joi.object().keys({
    protocol: Joi.string().required(),
    hostname: Joi.string().required(),
    pathname: Joi.string().required(),
    parameters: Joi.string().required(),
    indexable: Joi.boolean().required(),
  }),
};

/**
 * Validate given object with given schema structure
 * @param {Object} request
 * @param {String} schema
 */
const validate = (request, schema) =>
  Joi.validate(request, schemas[schema], { presence: 'required' });

module.exports = {
  validate,
};
